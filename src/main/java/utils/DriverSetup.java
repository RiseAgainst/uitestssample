package utils;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.ChromeDriverManager;


public class DriverSetup {
    private static String baseUrl = "https://yourUrlHere";

    public static void setupDriver() {
        Configuration.browser = "chrome";
        //Configuration.browser = "utils.CustomENLocaleWebDriver";
        Configuration.timeout = 8000; // base timeout between any actions like search,fill input (any browser action)
        Configuration.fastSetValue = false;
        Configuration.holdBrowserOpen = true;   // debug mode, opened browser after test is finished

        if(Configuration.baseUrl.equals("http://localhost:8080")) {
            Configuration.baseUrl = baseUrl ;
        }

        ChromeDriverManager.getInstance().setup();
    }

}
