package models;

import lombok.Data;

@Data
public class Company {
    String companyName;
    String companyRegNumber;
    String companyAddress;
    String companyPhone;
    String companyFax;

    public Company(String companyName, String companyRegNumber, String companyAddress, String companyPhone, String companyFax) {
        this.companyName = companyName;
        this.companyRegNumber = companyRegNumber;
        this.companyAddress = companyAddress;
        this.companyPhone = companyPhone;
        this.companyFax = companyFax;
    }
}
