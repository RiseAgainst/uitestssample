package models;

import lombok.Data;

@Data
public class BankInfo {
    String merchandiseBank;
    String bankAccountHolderName;
    String bankAccountNumber;
    String bankAccountRegistrationNumber;

    public BankInfo(String merchandiseBank, String bankAccountHolderName, String bankAccountNumber, String bankAccountRegistrationNumber) {
        this.merchandiseBank = merchandiseBank;
        this.bankAccountHolderName = bankAccountHolderName;
        this.bankAccountNumber = bankAccountNumber;
        this.bankAccountRegistrationNumber = bankAccountRegistrationNumber;
    }

}
