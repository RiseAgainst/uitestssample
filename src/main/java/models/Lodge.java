package models;

import lombok.Data;

@Data
public class Lodge {
    String lodgeName;
    String lodgeAdress;

    public Lodge(String lodgeName, String lodgeAdress) {
        this.lodgeName = lodgeName;
        this.lodgeAdress = lodgeAdress;
    }

}
