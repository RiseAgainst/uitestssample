package models;

import lombok.Data;

@Data
public class RoomData {
    String roomName;
    String guestNumber;
    String buildUnits;
    String defaultAllotment;

    public RoomData(String roomName, String guestNumber, String buildUnits, String defaultAllotment) {
        this.roomName = roomName;
        this.guestNumber = guestNumber;
        this.buildUnits = buildUnits;
        this.defaultAllotment = defaultAllotment;
    }
}
