package models;

import lombok.Data;

/**
 * Created by enclave on 20.12.2017.
 */
@Data
public class User {
    String name;
    String password;
    String designation;
    String phoneNumber;
    String email;
    String passportNumber;


    public User(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public User(String name, String password, String designation, String phoneNumber, String email, String passportNumber) {
        this.name = name;
        this.password = password;
        this.designation = designation;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.passportNumber = passportNumber;
    }



}
