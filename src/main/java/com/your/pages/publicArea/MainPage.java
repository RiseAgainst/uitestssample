package com.your.pages.publicArea;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class MainPage {
    private SelenideElement localeDropdown = $(By.xpath("//*[@data-target = '.subitem-language-menu']"));
    private SelenideElement englishItem = $(By.xpath("//*[text() = 'English']"));
    private SelenideElement signupButton = $(By.xpath("//*[text() = 'Sign Up']"));
    private SelenideElement advancedSignUp = $(By.xpath("//h3[text() = 'Advanced']"));

    public MainPage open() {
        Selenide.open("");
        changeLocaleToEn();
        return this;
    }

    public void changeLocaleToEn() {
        localeDropdown.click();
        englishItem.click();
    }

    public MainPage clickSignUpButton() {
        signupButton.click();
        return this;
    }
    public SignUpPage clickAdvancedSignUp() {
        advancedSignUp.click();
        return new SignUpPage();
    }
}
