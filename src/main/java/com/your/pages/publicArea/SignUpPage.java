package com.your.pages.publicArea;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import models.BankInfo;
import models.Company;
import models.Lodge;
import models.User;
import org.openqa.selenium.By;
import utils.Misc;

import static com.codeborne.selenide.Selenide.$;

public class SignUpPage {

    private SelenideElement companyName = $(By.id("company_name"));
    private SelenideElement companyRegNumber = $(By.id("company_reg_number"));
    private SelenideElement companyAdress = $(By.id("company_address"));
    private SelenideElement companyPhone= $(By.id("company_phone"));
    private SelenideElement companyFax = $(By.id("company_fax"));

    // Lodge info
    private SelenideElement lodgeCount = $(By.id("lodge-number"));
    private SelenideElement lodgeName = $(By.xpath("//*[@name = 'lodge0name']"));
    private SelenideElement lodgeAdress = $(By.xpath("//*[@name = 'lodge0address']"));
    private SelenideElement merchandiseBank = $(By.id("bankName"));
    private SelenideElement bankHolder = $(By.id("bank_holder"));
    private SelenideElement bankAccountNumber = $(By.id("bank_account_number"));
    private SelenideElement bankCompanyRegNum = $(By.id("bank_company_number"));
    // Percson in charge details
    private SelenideElement userName = $(By.id("user_name"));
    private SelenideElement userPassportNum = $(By.id("user_id_code"));
    private SelenideElement userDesignation = $(By.id("user_designation"));
    private SelenideElement userPhoneNumber = $(By.id("user_phone"));
    private SelenideElement userEmail = $(By.id("user_email"));
    private SelenideElement userPassword = $(By.id("user_password"));
    private SelenideElement userPasswordConfrimation = $(By.id("user_password_confirmation"));
    private SelenideElement domainName = $(By.xpath("//*[@ng-model = 'registrationData.userDomain']"));
    private SelenideElement termsOfUse = $(By.id("agreed"));
    private SelenideElement captcha = $(By.xpath("//*[@id='recaptcha-anchor']/div[5]"));
    private SelenideElement submit = $(By.xpath("//button[contains(text(), 'Signup For Free')]"));


    public SignUpPage open() {
        Selenide.open("");
        return  this;
    }

    public SignUpPage fillSignUpForm(Company company, Lodge lodge, BankInfo bankInfo, User user) {
        fillCompanyInfo(company);
        fillLodgeInfo(lodge);
        fillBankInfo(bankInfo);
        fillUserInfo(user);
        confirmTermsOfUse();
       //submitForm();
        return this;
    }

    public SignUpPage fillCompanyInfo(Company company) {
        companyName.val(company.getCompanyName());
        companyRegNumber.val(company.getCompanyRegNumber());
        companyAdress.val(company.getCompanyAddress());
        companyPhone.val(company.getCompanyPhone());
        companyFax.val(company.getCompanyFax());
        return this;
    }

    public SignUpPage setLodgesCount(int count) {
        lodgeCount.selectOption(count);
        return this;
    }

    public SignUpPage fillLodgeInfo(Lodge lodge) {
        lodgeName.val(lodge.getLodgeName());
        lodgeAdress.val(lodge.getLodgeAdress());
        return this;
    }

    public SignUpPage fillBankInfo (BankInfo bankInfo){
        merchandiseBank.val(bankInfo.getMerchandiseBank());
        bankHolder.val(bankInfo.getBankAccountHolderName());
        bankAccountNumber.val(bankInfo.getBankAccountNumber());
        bankCompanyRegNum.val(bankInfo.getBankAccountRegistrationNumber());
        return  this;
    }

    public SignUpPage fillUserInfo(User user) {
        userName.val(user.getName());
        userPassportNum.val(user.getPassportNumber());
        userDesignation.val(user.getDesignation());
        userPhoneNumber.val(user.getPhoneNumber());
        userEmail.val(user.getEmail());
        userPassword.val(user.getPassword());
        userPasswordConfrimation.val(user.getPassword());
        return this;
    }

    public SignUpPage confirmTermsOfUse() {
        termsOfUse.click();
        Misc.scrollToElement(submit);
        captcha.click();
        return this;
    }

    public SignUpPage submitForm() {
        submit.click();
        return this;
    }


}
