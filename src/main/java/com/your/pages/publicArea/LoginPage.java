package com.your.pages.publicArea;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.your.pages.lodgeManagement.Home;
import models.User;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by enclave on 20.12.2017.
 */
public class LoginPage {
    private SelenideElement login = $(By.id("user_email"));
    private SelenideElement password =  $(By.id("user_password"));
    private SelenideElement submit =  $(By.name("commit"));


    public LoginPage open() {
        Selenide.open("users/sign_in");
        return  this;
    }

    public Home loginAs(User user) {
        login.val(user.getName());
        password.val(user.getPassword());
        submit.click();
        return new Home();
    }

}
