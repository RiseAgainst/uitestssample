package com.your.pages.lodgeManagement;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MenuPanel {
    protected SelenideElement userMenu = $(By.id("nav-user-menu"));
    protected SelenideElement hotelSelector = $(By.className("selected-lodge-name"));
    protected SelenideElement homeButton = $(By.xpath("//*[contains(text(), 'Home')]"));
    protected SelenideElement bookingButton = $(By.xpath("//*[contains(text(), 'Booking')]"));
    protected SelenideElement calendarButton = $(By.xpath("//*[contains(text(), 'Calendar')]"));
    protected SelenideElement setupButton = $(By.xpath("//*[contains(text(), 'Setup')]"));
    protected SelenideElement moreButton = $(By.xpath("//a[contains(text(), 'More')]"));
    protected SelenideElement moreRoom = $(By.xpath("//*[contains(text(), 'Room')]"));
    protected SelenideElement morePrice = $(By.xpath("//*[contains(text(), 'Price')]"));
    protected SelenideElement moreSettling = $(By.xpath("//*[contains(text(), 'Settling')]"));
    protected SelenideElement moreUsers = $(By.xpath("//*[contains(text(), 'Users')]"));
    protected SelenideElement moreSell = $(By.xpath("//*[contains(text(), 'Sell')]"));
    protected SelenideElement morePayment = $(By.xpath("//*[contains(text(), 'Payment')]"));
    protected SelenideElement moreHowTo = $(By.xpath("//*[contains(text(), 'How To')]"));

    public Room clickRoom() {
        moreButton.click();
        moreRoom.click();
        return new Room();
    }





}
