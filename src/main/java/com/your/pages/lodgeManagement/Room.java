package com.your.pages.lodgeManagement;

import com.codeborne.selenide.SelenideElement;
import models.RoomData;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class Room {
    private SelenideElement addNewRoom = $(By.id("add-room"));
    private SelenideElement saveChanges = $(By.id("save_room"));
    private SelenideElement cancel = $(By.xpath("//button[text() = 'Cancel']"));
    private SelenideElement status = $(By.name("status"));
    private SelenideElement roomName = $(By.name("status"));
    private SelenideElement maxGuests = $(By.name("maxGuests"));
    private SelenideElement buildUnits = $(By.name("roomsCount"));
    private SelenideElement defaultAllotment = $(By.name("allotment"));
    private SelenideElement photoUploader = $(By.xpath("//*[@class = 'dropzone-wrapper dz-clickable']"));
    private SelenideElement roomUnit;

    // description


  public Room clickAddNewRoom() {
      addNewRoom.click();
      return this;
  }

  public Room getRoomUnit(String roomId) {
      this.roomUnit = $(By.xpath("//*[@data-room-id = '" +  roomId + "']"));
      return this;
  }

  public Room fillRoomData(RoomData roomData) {
      setRoomName(roomData.getRoomName());
      setMaxGuests(roomData.getGuestNumber());
      setBuildUnits(roomData.getBuildUnits());
      setDefaultAllotment(roomData.getDefaultAllotment());
      return this;
  }

  public Room setStatus(String statusText) {
      status.selectOption(statusText);
      return this;
  }

  public Room setRoomName(String roomNameText) {
      roomName.val(roomNameText);
      return this;
  }

    public Room setMaxGuests(String maxGuestsValue) {
        maxGuests.val(maxGuestsValue);
        return this;
    }

    public Room setBuildUnits(String buildUnitsValue) {
       buildUnits.val(buildUnitsValue);
       return this;
    }

    public Room setDefaultAllotment(String defaultAllotmentValue) {
      defaultAllotment.val(defaultAllotmentValue);
      return this;
    }

    public Room uploadPhoto(String... fileName) {
        String separator = System.getProperty("file.separator");
        File[] files = new File[fileName.length];
        for(int i = 0; i < fileName.length; i++) {
            files[i] = new File(System.getProperty("user.dir") + separator + "src" + separator + "test" + separator + "resources" +  separator + "testfiles" + separator  + fileName[i]);
            photoUploader.uploadFile(files[i]);
        }
        return this;
    }

    public Room clickSaveChanges() {
        saveChanges.click();
        return this;
    }

    public Room clickCancel() {
        cancel.click();
        return this;
    }


}
