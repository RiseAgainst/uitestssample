package BaseClasses;


import org.testng.annotations.BeforeClass;

import org.testng.annotations.Listeners;
import utils.DriverSetup;

import static com.codeborne.selenide.Selenide.$;

@Listeners({RunTestMethodAndTakeAttachement.class})
public class Base {

    @BeforeClass
    public void setUp() {
        DriverSetup.setupDriver();
    }



}
