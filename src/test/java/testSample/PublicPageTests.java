package testSample;

import BaseClasses.Base;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;


@Features("Feature block name")
public class PublicPageTests extends Base {

// Your tests here
    @Test
    @Stories("Test can open google")
    public void testCanOpenGoogle() {
        open("http://google.com");
    }

    @Test
    @Stories("Test can search in google")
    public void testCanSearchInGoogle() {
        open("http://google.com");
        $(By.name("q")).val("selenide").pressEnter();
        $$("#ires .g").shouldHave(size(10));
        $("#ires .g").shouldHave(text("selenide.org"));
    }



}
