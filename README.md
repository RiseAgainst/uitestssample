To run test:
gradlew test  
to run custom testng configuration gradlew test -Pregression1 

Used technologies:  
- Java 8  
- TestNg  
- Selenide  
- WebDriverManager  
- Allure Report  
- Gradle  
- Lombok  

To make Lombok work for Intelij Idea:  

1. Preferences (Ctrl + Alt + S)  
Build, Execution, Deployment  
Compiler  
Annotation Processors  
Enable annotation processing  
2. Make sure you have the Lombok plugin for IntelliJ installed!  
Preferences -> Plugins  
Search for "Lombok Plugin"  
Click Browse repositories...  
Choose Lombok Plugin  
Install  
Restart IntelliJ  

